Messenger.js
============

Custom event-like class

### Usage

```javascript
	// create a new Messenger instance
	var messenger = new Messenger();

	// listen for a message: 'HelloWorld' and attach a callback: hello
	messenger.listen('HelloWorld', hello);

	function hello(data) {
		console.log(data.text);
	}

	var dataToSend = {text:'Hello World!'}

	// notify with a message: 'HelloWorld' and [optional] data:
	messenger.notify('HelloWorld', dataToSend);
```
